$( document ).ready(function() {
    //hide/show tabs content
    $(function(){
        $('.tab-item').not(':first').hide();
        $('.tab').click(function(){
        $('.tab').removeClass('tab-active').eq($(this).index()).addClass('tab-active');
        $('.tab-item').hide().eq($(this).index()).fadeIn(500);
        }).eq(0).addClass('tab-active');
    });

    $(function(){
        $('.tab-item_inner').not(':first').hide();
        $('.tab_inner').click(function(){
        $('.tab_inner').removeClass('tab-active').eq($(this).index()).addClass('tab-active');
        $('.tab_inner').removeClass('tab-valid').eq($(this).index()).prevAll().addClass('tab-valid');
        $('.tab-item_inner').hide().eq($(this).index()).fadeIn(500);
        }).eq(0).addClass('tab-active');
    });

    //hide/open header dropdown menu
    $('.header .dropdown a').click(function(event){
        event.preventDefault();
        $(this).next('ul').toggleClass('active');
        $('li.dropdown a').toggleClass('active');
    });

    //hide/open left menu
    $('.content nav a.menu-size').click(function(event){
        event.preventDefault();
        $(this).toggleClass('active');
        $('nav').toggleClass('active');
    });
});